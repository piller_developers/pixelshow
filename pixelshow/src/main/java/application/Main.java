package application;

import java.awt.image.BufferedImage;
import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Main extends Application implements EventHandler<ActionEvent> {
    Button btnBildAussuchen;
    Slider sldrPixel;
    BufferedImage image;
    File file;
    HBox hbxControlling;
    Scene Buttonscene;
    BorderPane bpImage;
    Stage window;
    Button btnStart;
    Pixels pixels;

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;

        primaryStage.setTitle("Test");

        btnBildAussuchen = new Button("Bild ausw�hlen");
        sldrPixel = new Slider();
        sldrPixel.setMaxWidth(400);
        sldrPixel.setMaxHeight(50);
        sldrPixel.setLayoutX(50);
        sldrPixel.setMin(4);
        sldrPixel.setMax(100);
        sldrPixel.setValue(20);
        sldrPixel.setManaged(true);
        sldrPixel.setShowTickLabels(true);

        btnStart = new Button("Starten");

        pixels = new Pixels();

        UIStart();
    }

    public void handle(ActionEvent event) {
        if (event.getSource().equals(btnBildAussuchen)) {
            btnBildAussuchen.setDisable(true);

            String userDirectory = System.getProperty("user.home");
            // "D:\WORKSPACES\workspace_VBL\maven.1536139228188\pixelshow\src\main\resourcess"

            file = pixels.getFile(window, userDirectory, "Select an image");

            if (file == null) {
                btnBildAussuchen.setDisable(false);
                return;
            }
            btnBildAussuchen.setDisable(false);

        } else if (event.getSource().equals(btnStart)) {
            btnStart.setDisable(true);
            int x = (int) bpImage.getWidth();
            int y = (int) bpImage.getHeight();

            bpImage.getChildren().clear();
            UIStart();

            try {
                pixels.makePixels(file, (int) sldrPixel.getValue(), bpImage, x, y);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
            btnStart.setDisable(false);
            hbxControlling.toFront();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void UIStart() {
        hbxControlling = new HBox();

        hbxControlling.setSpacing(10);

        hbxControlling.getChildren().add(btnBildAussuchen);
        hbxControlling.getChildren().add(sldrPixel);
        hbxControlling.getChildren().add(btnStart);

        hbxControlling.setAlignment(Pos.TOP_CENTER);

        btnBildAussuchen.setOnAction(this);
        btnStart.setOnAction(this);

        bpImage = new BorderPane();
        bpImage.setBottom(hbxControlling);

        Buttonscene = new Scene(bpImage);

        window.setScene(Buttonscene);
        window.show();
        window.setFullScreen(true);
    }
}
