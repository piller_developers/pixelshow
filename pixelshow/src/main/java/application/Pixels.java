package application;

// catches IO Exception(reading Error of the File)
import java.awt.image.BufferedImage;
import java.io.File;
// Creates File Object
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Slider;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;

public class Pixels extends Application {
    Group root;
    Scene imageScene;
    Button btnBildAussuchen;
    Slider sldrPixelStaerke;
    BufferedImage image = null;
    File file = null;

    public static void main(String args[]) throws IOException {
        launch(args);

    }

    public void start(final Stage primaryStage) throws IOException {
    }

    // returns the RGB from x and y
    private RGB GetRGBFromImage(BufferedImage img, int x, int y) {
        int rgb = img.getRGB(x, y);

        int red = (rgb & 0x00ff0000) >> 16;
        int green = (rgb & 0x0000ff00) >> 8;
        int blue = rgb & 0x000000ff;
        return (new RGB(red, green, blue));
    }

    // returns an int from the userinput
    public int GetUserInputInt() {
        Scanner Userinput = new Scanner(System.in);
        boolean loop = false;

        do {
            if (Userinput.hasNextInt()) {
                loop = true;
            } else {
                System.out.println("Falscher Wert, bitte erneut eingeben");
                loop = false;
                Userinput.next();
            }
        } while (!(loop));

        return Userinput.nextInt();
    }

    // shows a Filechooser
    public File getFile(Stage stg, String startpath, String title) {
        try {

            FileChooser fc = new FileChooser();
            file = null;

            fc.setTitle(title);

            // Filter, damit nur JPG Dateien ausgew�hlt werden k�nnen
            ExtensionFilter filter = new ExtensionFilter("JPG files (*.jpg)", "*.jpg");
            fc.getExtensionFilters().add(filter);
            fc.setInitialDirectory(new File(startpath));

            // Filedialog anzeigen
            file = fc.showOpenDialog(stg);

            return file;
        } catch (Exception e) {
            throw e;
        }
    }

    public void makePixels(File file, int everypixel, BorderPane hbox, int screenwidth, int screenHeight) {
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int yheight = image.getHeight();
        int xwidth = image.getWidth();

        int offsetX = screenwidth / 2 - xwidth / 2;
        int offsetY = screenHeight / 2 - yheight / 2;

        for (int y = 0; y < yheight; y = y + (everypixel)) {
            for (int x = 0; x < xwidth; x = x + (everypixel)) {
                RGB rgb = GetRGBFromImage(image, x, y);

                Rectangle square = new Rectangle();
                square.setX(x + offsetX);
                square.setY(y + offsetY);
                square.setWidth(everypixel);
                square.setHeight(everypixel);

                square.setFill(javafx.scene.paint.Color.rgb(rgb.getRed(), rgb.getGreen(), rgb.getBlue()));

                hbox.getChildren().add(square);
            }
        }
    }
}// class ends here
